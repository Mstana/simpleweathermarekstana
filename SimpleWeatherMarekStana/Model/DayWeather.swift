//
//  DayWeather.swift
//  SimpleWeatherMarekStana
//
//  Created by Marek Stana on 16/01/16.
//  Copyright © 2016 Marek Stana. All rights reserved.
//

import Foundation

class DayWeather {
    
    
    var temperature:Int?
    var windSpeed:Double?
    var windDirection:String?
    var weatherConditions:String?
    
    
    init() {
        
    }
    
    init(temperature:Int, windSpeed:Double, windDirection:Float, weatherConditions:String ) {
        
        self.temperature = temperature
        self.windSpeed = windSpeed
        self.windDirection = degToCompass(windDirection)
        self.weatherConditions = weatherConditions
        
    }
    
    
    func degToCompass(degree:Float) -> String {
        
        let val = Int((degree/22.5) + 0.5)
        
        let arr = ["N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"]
        
        return arr[(val % 16)]
    }
    
    func getStringTemperature() -> String {
        
        return (temperature?.description)! + " °C"
    }
    
    func getStringWindSpeed() -> String {
        
        return (windSpeed?.description)! + " km/h"
    }
}
