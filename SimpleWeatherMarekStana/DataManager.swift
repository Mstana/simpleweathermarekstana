//
//  DataManager.swift
//  SimpleWeatherMarekStana
//
//  Created by Marek Stana on 16/01/16.
//  Copyright © 2016 Marek Stana. All rights reserved.
//

import Alamofire
import SwiftyJSON


class DataManager {
    
    var key: String = "3f6bd4ab0371a12a7ad29559033bf850"
    var lat: String = "49.201198"
    var long: String = "16.606829"
    
    class var sharedInstance: DataManager {
        struct Static {
            static let instance: DataManager = DataManager()
        }
        return Static.instance
    }
 
    func getDayWeather(responseHandler: (dw:DayWeather?) -> ()) {
        
        Alamofire.request(.GET, "http://api.openweathermap.org/data/2.5/weather", parameters:[
            "APPID": key,
            "lat": lat,
            "lon": long,
            "units": "metric"])
            .responseJSON { response in
                if let j = response.result.value {
                    print(j)
                    let json = JSON(j)
                    let dw = DayWeather(
                        temperature: json["main"]["temp"].intValue,
                        windSpeed: json["wind"]["speed"].doubleValue,
                        windDirection: json["wind"]["deg"].floatValue,
                        weatherConditions: json["weather"][0]["main"].stringValue
                    )
                    responseHandler(dw: dw)
                } else {
                    responseHandler(dw: nil)
                }
        }
    }
    
    
    func getTemperatureFromString(temperature:String) -> String {
        
        return temperature.componentsSeparatedByString(".")[0]
    }
    
    
}
