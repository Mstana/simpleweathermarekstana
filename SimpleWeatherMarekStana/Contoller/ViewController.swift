//
//  ViewController.swift
//  SimpleWeatherMarekStana
//
//  Created by Marek Stana on 16/01/16.
//  Copyright © 2016 Marek Stana. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    
    @IBOutlet weak var weatherConditionsLabel: UILabel!
    
    @IBOutlet weak var windDirectionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dataManager: DataManager = DataManager.sharedInstance
        
        dataManager.getDayWeather() { (dw) -> () in
            
            if dw != nil {
                
                self.temperatureLabel.text = dw?.getStringTemperature()
                self.windSpeedLabel.text = "Wind Speed: " + (dw?.windSpeed?.description)!
                self.weatherConditionsLabel.text = "Weather Conditions: " + (dw?.getStringWindSpeed())!
                self.windDirectionLabel.text = "Wind Direction: " + (dw?.windDirection)!
            } else {
                self.temperatureLabel.text = ""
                self.windSpeedLabel.text = ""
                self.weatherConditionsLabel.text = ""
                self.windDirectionLabel.text = ""
                self.noDataAlert()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func degToCompass(degree:Float) -> String {
        
        let val = Int((degree/22.5) + 0.5)
        
        let arr = ["N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"]
        
        return arr[(val % 16)]
    }
    
    func noDataAlert() {
        
        let alertController = UIAlertController(
            title: "Unable to download data",
            message: "Connect to internet to see weather.",
            preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
}

